package az.etibarli.excelexport.service;

import az.etibarli.excelexport.domain.Customer;
import az.etibarli.excelexport.repository.CustomerRepository;
import az.etibarli.excelexport.utils.ExcelExportUtils;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService{

    private final CustomerRepository repository;

    @Override
    public List<Customer> exportCustomerToExcel(HttpServletResponse response) throws Exception {
        List<Customer> customers = repository.findAll();
        ExcelExportUtils excelExportUtils = new ExcelExportUtils(customers);
        excelExportUtils.exportDataToExcel(response);
        return customers;
    }

}
