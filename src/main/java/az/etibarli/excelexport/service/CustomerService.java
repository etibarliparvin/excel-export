package az.etibarli.excelexport.service;

import az.etibarli.excelexport.domain.Customer;
import jakarta.servlet.http.HttpServletResponse;

import java.util.List;

public interface CustomerService {

    List<Customer> exportCustomerToExcel(HttpServletResponse response) throws Exception;

}
