package az.etibarli.excelexport.repository;

import az.etibarli.excelexport.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
